import Vue from 'vue';
import VueRouter from 'vue-router';
import ProductList from '@/views/ProductList.vue';
import Order from '@/views/Order.vue';
import User from '@/views/User.vue';
import Product from '@/views/Product.vue';
import Home from '../views/Home.vue';
import OrdersList from '../views/OrdersList.vue';
import Inscription from '../views/Inscription.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/products',
    name: 'ProductList',
    component: ProductList,
  },
  {
    path: '/orders',
    name: 'Order',
    component: Order,
  },
  {
    path: '/users',
    name: 'User',
    component: User,
  },
  {
    path: '/products/:id',
    name: 'Product',
    component: Product,
  },
  {
    path: '/ordersList',
    name: 'List',
    component: OrdersList,
  },
  {
    path: '/register',
    name: 'Register',
    component: Inscription,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
