import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    counter: 0,
  },
  getters: {
    isConnected(state) {
      if (state.token === null) {
        return false;
      }
      return true;
    },
  },
  mutations: {
    setToken(state, newToken) {
      state.token = newToken;
      localStorage.setItem('token', newToken);
      Axios.defaults.headers.Authorization = `Bearer ${newToken}`;
    },
    logout(state) {
      state.token = null;
      localStorage.removeItem('token');
      Axios.defaults.headers.Authorization = null;
    },
    changeCounter(state, n) {
      state.counter += n;
    },
  },
  actions: {
  },
  modules: {
  },
});
