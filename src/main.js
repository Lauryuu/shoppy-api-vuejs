import Vue from 'vue';
import Axios from 'axios';
import App from './App.vue';
import router from './router';
import store from './store';

Axios.defaults.baseURL = process.env.VUE_APP_URL;

const token = localStorage.getItem('token');
if (token) {
  store.commit('setToken', token);
}

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
